#+title: laravel 8.x crud
#+author: mit
#+EMAIL: your@mail.address
#+category: programming
#+startup:
#+tags:

#+OPTIONS: ^:nil


#+setupfile: /home/mit/Documents/org/org-macros.setup
# //  フォントの色の変更，ハイライト https://taipapamotohus.com/post/html_export/

#+HTML_HEAD: <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Inconsolata:400,700" rel="stylesheet" type="text/css" />
# #+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/thi-ng/org-spec/master/css/style.css">
#+HTML_HEAD: <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Inconsolata:400,700" rel="stylesheet" type="text/css" />

#+HTML: <div class="outline-2" id="meta">
| *Author* | {{{author}}} ({{{email}}})    |
| *Date*   | {{{time(%Y-%m-%d %H:%M:%S)}}} |
#+HTML: </div>

# +INFOJS_OPT: view:info toc:t path:http://orgmode.org/org-info.js mouse:underline
# view: info/overview/content/showall
# #+TOC: headlines 2

#+BEGIN_COMMENT
https://qiita.com/deco/items/997b25a3bd09baaa8d8a
https://takaxp.github.io/org-ja.html

C-u C-u C-u TAB	--> すべてを展開して表示

C-c C-e h	--> (org-export-as-html)
C-c C-e h h	--> write html
C-c C-e h o     --> Open with Browser

C-c C-s	--> SCHEDULED: <2012-11-15 木>
C-c C-d	--> DEADLINE: <2012-11-16 金>
C-c .	--> <2012-11-16 金>
C-u C-c .	--> <2012-11-16 金 22:11>

# org-structure-template-alist
<c Tab	--> tempo-template-org-comment
<s Tab	--> tempo-template-org-comment
        i(index), A(ascii), H(html), L(latex), v(verse), s(src), q(quote),
        E(export), l(export latex), h(export html), e(example), C(comment), c(center), I(include)...

<>(active), [](no-active)

 Repeat(d,w,m,y) <2019-11-17 Sun 12:30 +1y>
 Range <2020-11-17 10:00>--<2020-11-19 12:00>




日時（所要時間）
場所
参加者（担当者）
議題
配布資料の有無


#+END_COMMENT





Refer
https://dev.to/kingsconsult/laravel-8-crud-bi9

* Step 1: Install Laravel 8
To install Laravel 8, we will need a composer and we make sure we specify the version of Laravel we need, in our case Laravel 8.

#+BEGIN_SRC
composer create-project laravel/laravel=8.0 --prefer-dist projectapp
#+END_SRC

** Another important thing about Laravel 8, you don’t need to generate APP_KEY, this new version will also generate it for us.
#+BEGIN_SRC
laravel new projectapp

php artisan key:generate --ansi
#+END_SRC


* Step 2: Database setup
edit .env, copy from .env.example.

#+BEGIN_SRC
...
DB_CONNECTION=sqlite
#DB_CONNECTION=mysql
#DB_HOST=127.0.0.1
#DB_PORT=3306
#DB_DATABASE=laravel
#DB_USERNAME=root
#DB_PASSWORD=
...
#+END_SRC

#+BEGIN_SRC
DB_CONNECTION=sqlite
DB_DATABASE=laravel
#+END_SRC

#+BEGIN_SRC
touch database/database.sqlite
#+END_SRC


* Step 3: Create Migration

#+BEGIN_SRC
php artisan make:migration create_projects_table --create=projects
#+END_SRC

app/Providers/AppServiceProvider.php
#+BEGIN_SRC
<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;               // <------
use Illuminate\Support\Facades\Schema;


class AppServiceProvider extends ServiceProvider
{
...
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
    Schema::defaultStringLength(191);                 // <------
    }
}
#+END_SRC

#+BEGIN_SRC
php artisan migrate
#+END_SRC

* Step 4: Add Resource Route

resource/web.php
#+BEGIN_SRC
Route::resource(‘projects’, ProjectController::class);
#+END_SRC


#+BEGIN_SRC
use App\Http\Controllers\ProjectController;
#+END_SRC

* Step 5: Add Controller and Model
Previously, in step 4, the second parameter for the resource is ProjectController, so we need to create the controller and we need to specify the resource model by add --model

#+BEGIN_SRC
php artisan make:controller ProjectController --resource --model=Project
#+END_SRC

app/Http/Controllers/ProjectController.php
- index()
- create()
- store(Request, $request)
- show(Project, $project)
- edit(Project, $project)
- update(Request, $request, Project, $project)
- destroy( Project, $project)

#+BEGIN_SRC
<?php
namespace App\Http\Controllers;

use App\Models\Project;
use Illuminate\Http\Request;

class ProjectController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $projects = Project::latest()->paginate(5);

        return view('projects.index', compact('projects'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('projects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'introduction' => 'required',
            'location' => 'required',
            'cost' => 'required'
        ]);

        Project::create($request->all());

        return redirect()->route('projects.index')
            ->with('success', 'Project created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function show(Project $project)
    {
        return view('projects.show', compact('project'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function edit(Project $project)
    {
        return view('projects.edit', compact('project'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Project $project)
    {
        $request->validate([
            'name' => 'required',
            'introduction' => 'required',
            'location' => 'required',
            'cost' => 'required'
        ]);
        $project->update($request->all());

        return redirect()->route('projects.index')
            ->with('success', 'Project updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Project  $project
     * @return \Illuminate\Http\Response
     */
    public function destroy(Project $project)
    {
        $project->delete();

        return redirect()->route('projects.index')
            ->with('success', 'Project deleted successfully');
    }
}
#+END_SRC

* Step 6: Add your views
Laravel view files are is called the blade files, we are going to add those blade files, so a user can be able to interact with our app
I like arranging my views according to the models, so I am going to create two folders in the resources/views folder

- Layouts
 - app.blade.php
- Projects
 - Index.blade.php
 - Create.blade.php
 - Edit.blade.php
 - show.blade.php

** App.blade.php
#+BEGIN_SRC
<html>

<head>
    <title>App Name - @yield('title')</title>

    <!-- Bootstrap -->
    <link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-alpha/css/bootstrap.css" rel="stylesheet">

    <!-- Font Awesome JS -->
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/solid.js"
        integrity="sha384-tzzSw1/Vo+0N5UhStP3bvwWPq+uvzCMfrN1fEFe+xBmv1C/AtVX5K0uZtmcHitFZ" crossorigin="anonymous">
    </script>
    <script defer src="https://use.fontawesome.com/releases/v5.0.13/js/fontawesome.js"integrity="sha384-6OIrr52G08NpOFSZdxxz1xdNSndlD4vdcf/q2myIUVO0VsqaGHJsB0RaBE01VTOY" crossorigin="anonymous">
    </script>

    <style>
        .footer {
            position: fixed;
            left: 0;
            bottom: 0;
            width: 100%;
            background-color: #9C27B0;
            color: white;
            text-align: center;
        }

    </style>

</head>

<body>
    @section('sidebar')

    @show

    <div class="container">
        @yield('content')
    </div>
    <div class="text-center footer">
    </div>
</body>

</html>
#+END_SRC

** index.blade.php
#+BEGIN_SRC
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Laravel 8 CRUD </h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-success" href="{{ route('projects.create') }}" title="Create a project"> <i class="fas fa-plus-circle"></i>
                    </a>
            </div>
        </div>
    </div>

    @if ($message = Session::get('success'))
        <div class="alert alert-success">
            <p>{{ $message }}</p>
        </div>
    @endif

    <table class="table table-bordered table-responsive-lg">
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Introduction</th>
            <th>Location</th>
            <th>Cost</th>
            <th>Date Created</th>
            <th width="280px">Action</th>
        </tr>
        @foreach ($projects as $project)
            <tr>
                <td>{{ ++$i }}</td>
                <td>{{ $project->name }}</td>
                <td>{{ $project->introduction }}</td>
                <td>{{ $project->location }}</td>
                <td>{{ $project->cost }}</td>
                <td>{{ date_format($project->created_at, 'jS M Y') }}</td>
                <td>
                    <form action="{{ route('projects.destroy', $project->id) }}" method="POST">

                        <a href="{{ route('projects.show', $project->id) }}" title="show">
                            <i class="fas fa-eye text-success  fa-lg"></i>
                        </a>

                        <a href="{{ route('projects.edit', $project->id) }}">
                            <i class="fas fa-edit  fa-lg"></i>

                        </a>

                        @csrf
                        @method('DELETE')

                        <button type="submit" title="delete" style="border: none; background-color:transparent;">
                            <i class="fas fa-trash fa-lg text-danger"></i>

                        </button>
                    </form>
                </td>
            </tr>
        @endforeach
    </table>

    {!! $projects->links() !!}

@endsection
#+END_SRC

** create.blade.php
#+BEGIN_SRC
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Add New Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('projects.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('projects.store') }}" method="POST" >
        @csrf

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Introduction:</strong>
                    <textarea class="form-control" style="height:50px" name="introduction"
                        placeholder="Introduction"></textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Location:</strong>
                    <input type="text" name="location" class="form-control" placeholder="Location">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Cost:</strong>
                    <input type="number" name="cost" class="form-control" placeholder="Cost">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
#+END_SRC

** edit.blade.php
#+BEGIN_SRC
@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>Edit Product</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('projects.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <form action="{{ route('projects.update', $project->id) }}" method="POST">
        @csrf
        @method('PUT')

        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Name:</strong>
                    <input type="text" name="name" value="{{ $project->name }}" class="form-control" placeholder="Name">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Introduction:</strong>
                    <textarea class="form-control" style="height:50px" name="introduction"
                        placeholder="Introduction">{{ $project->introduction }}</textarea>
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Location:</strong>
                    <input type="text" name="location" class="form-control" placeholder="{{ $project->location }}"
                        value="{{ $project->location }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Cost:</strong>
                    <input type="number" name="cost" class="form-control" placeholder="{{ $project->cost }}"
                        value="{{ $project->location }}">
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                <button type="submit" class="btn btn-primary">Submit</button>
            </div>
        </div>

    </form>
@endsection
#+END_SRC

** show.blade.php
#+BEGIN_SRC
@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-12 margin-tb">
            <div class="pull-left">
                <h2>  {{ $project->name }}</h2>
            </div>
            <div class="pull-right">
                <a class="btn btn-primary" href="{{ route('projects.index') }}" title="Go back"> <i class="fas fa-backward "></i> </a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Name:</strong>
                {{ $project->name }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Introduction:</strong>
                {{ $project->introduction }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Location:</strong>
                {{ $project->location }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Cost:</strong>
                {{ $project->cost }}
            </div>
        </div>
        <div class="col-xs-12 col-sm-12 col-md-12">
            <div class="form-group">
                <strong>Date Created:</strong>
                {{ date_format($project->created_at, 'jS M Y') }}
            </div>
        </div>
    </div>
@endsection
#+END_SRC

** execute server
#+BEGIN_SRC
php artisan serve [--host 0.0.0.0] [--port 8080]
#+END_SRC

** access

http://127.0.0.1:8000/projects/

http://127.0.0.1:8000/projects/create

http://127.0.0.1:8000/projects/1/edit

http://127.0.0.1:8000/projects/1
