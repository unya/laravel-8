#+title: laravel 8.x test(curl)
#+author: mit
#+EMAIL: your@mail.address
#+category: programming
#+startup:
#+tags:

#+OPTIONS: ^:nil


#+setupfile: /home/mit/Documents/org/org-macros.setup
# //  フォントの色の変更，ハイライト https://taipapamotohus.com/post/html_export/

#+HTML_HEAD: <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Inconsolata:400,700" rel="stylesheet" type="text/css" />
# #+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://raw.githubusercontent.com/thi-ng/org-spec/master/css/style.css">
#+HTML_HEAD: <link href="http://fonts.googleapis.com/css?family=Roboto+Slab:400,700|Inconsolata:400,700" rel="stylesheet" type="text/css" />

#+HTML: <div class="outline-2" id="meta">
| *Author* | {{{author}}} ({{{email}}})    |
| *Date*   | {{{time(%Y-%m-%d %H:%M:%S)}}} |
#+HTML: </div>

# +INFOJS_OPT: view:info toc:t path:http://orgmode.org/org-info.js mouse:underline
# view: info/overview/content/showall
# #+TOC: headlines 2

#+BEGIN_COMMENT
https://qiita.com/deco/items/997b25a3bd09baaa8d8a
https://takaxp.github.io/org-ja.html

C-u C-u C-u TAB	--> すべてを展開して表示

C-c C-e h	--> (org-export-as-html)
C-c C-e h h	--> write html
C-c C-e h o     --> Open with Browser

C-c C-s	--> SCHEDULED: <2012-11-15 木>
C-c C-d	--> DEADLINE: <2012-11-16 金>
C-c .	--> <2012-11-16 金>
C-u C-c .	--> <2012-11-16 金 22:11>

# org-structure-template-alist
<c Tab	--> tempo-template-org-comment
<s Tab	--> tempo-template-org-comment
        i(index), A(ascii), H(html), L(latex), v(verse), s(src), q(quote),
        E(export), l(export latex), h(export html), e(example), C(comment), c(center), I(include)...

<>(active), [](no-active)

 Repeat(d,w,m,y) <2019-11-17 Sun 12:30 +1y>
 Range <2020-11-17 10:00>--<2020-11-19 12:00>




日時（所要時間）
場所
参加者（担当者）
議題
配布資料の有無


#+END_COMMENT


* curl


#+BEGIN_SRC
 curl --header 'Content-Type: application/json' \
        --request POST \
        --data '{ "name": "test", "email": "test@email.com", "password": "secret", "password_confirmation": "secret" }' \
        http://localhost:8000/api/register
#+END_SRC

#+BEGIN_SRC
 curl --header 'Content-Type: application/json' \
        --request POST \
        --data '{ "email": "test@test.com", "password": "test" }' \
        http://localhost:8000/api/login
#+END_SRC

#+BEGIN_SRC
## non Auth data
 curl --header 'Content-Type: application/json' \
        http://localhost:8000/api/open

setenv token
#+END_SRC

#+BEGIN_SRC
## Auth data
 curl --header 'Content-Type: application/json' \
      --header "Authorization: Bearer $TOKEN" \
        http://localhost:8000/api/user

## Auth data
 curl --header 'Content-Type: application/json' \
      --header "Authorization: Bearer $TOKEN" \
        http://localhost:8000/api/closed
#+END_SRC
